from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('Hobbies/', views.Hobbies, name='Hobbies'),
    path('game/', views.game, name='game'),
    path('contact/', views.contact, name='contact'),
]