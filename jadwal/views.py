from django.shortcuts import render, redirect
from . import forms, models
from .forms import formulir
from .models import Jadwal
# Create your views here.
def formulir_jadwal(request):
    if(request.method == "POST"):
        form = forms.formulir(request.POST)
        if(form.is_valid()):
            form2 = models.Jadwal()
            form2.Matkul = form.cleaned_data["Matkul"]
            form2.Dosen = form.cleaned_data["Dosen"]
            form2.SKS = form.cleaned_data["SKS"]
            form2.Jam = form.cleaned_data["Jam"]
            form2.Semester = form.cleaned_data["Semester"]
            form2.save()
        return redirect("/jadwal")
    else:
        form = forms.formulir()
        form2 = models.Jadwal.objects.all()
        form_dictio = {
            'formulir' : form,
            'jadwal' : form2
        }
        return render(request, 'jadwal.html', form_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        form = forms.formulir(request.POST)
        if(form.is_valid()):
            form2 = models.Jadwal()
            form2.Matkul = form.cleaned_data["Matkul"]
            form2.Dosen = form.cleaned_data["Dosen"]
            form2.SKS = form.cleaned_data["SKS"]
            form2.Jam = form.cleaned_data["Jam"]
            form2.Semester = form.cleaned_data["Semester"]
            form2.save()
        return redirect("/jadwal")
    else:
        models.Jadwal.objects.filter(pk = pk).delete()
        form = forms.formulir()
        form2 = models.Jadwal.objects.all()
        form_dictio = {
            'formulir' : form,
            'jadwal' : form2
        }
        return render(request, 'jadwal.html', form_dictio)

def detail(request, pk):
    matkul = Jadwal.objects.get(pk=pk)
    detail_dictio= {
        "matkul" : matkul,

    }
    return render(request, 'detail.html', detail_dictio)


