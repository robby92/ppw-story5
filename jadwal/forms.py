from django import forms

class formulir(forms.Form):
    Matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))
    Dosen = forms.CharField(widget=forms.DateTimeInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required' : True
    }))
    SKS = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True
    }))
    Jam = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Waktu mata kuliah berlangsung',
        'type' : 'text',
        'required' : True
    }))
    Semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Diambil pada semester berapa',
        'type' : 'text',
        'required' : True
    }))
   
