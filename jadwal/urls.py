from django.urls import path
from . import views
from .views import formulir_jadwal, delete, detail


app_name = "jadwal"

urlpatterns = [
    path('', views.formulir_jadwal),
    path('<int:pk>', delete, name = 'delete'),
    path('jadwal/',formulir_jadwal, name = 'jadwal'),
    path('detail/<int:pk>', detail, name='detail'),
]