from django.db import models

# Create your models here.
class Jadwal(models.Model):
    Matkul = models.CharField(blank=False, max_length= 100)
    Dosen = models.CharField(blank=False, max_length= 100)
    SKS = models.CharField(blank=False, max_length= 100)
    Jam = models.CharField(blank=False,max_length= 100)
    Semester = models.CharField(blank=False,max_length= 100)